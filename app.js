'use strict';

process.env.NODE_ENV = process.env.NODE_ENV || 'development';
process.env.CONST_FILE = process.env.CONST_FILE || 'constants';
let express = require('express');
let app = express();
let bodyParser = require('body-parser');
let mongoose = require('mongoose');
let Promise = require('bluebird');
let config = require(`./config/${process.env.NODE_ENV}.js`);
let constants = require('./config/constants'); 
let compression = require('compression');
const error = require('./app/helpers/error');
var schedule = require('node-schedule');
let requestify = require('requestify');
let _ = require('underscore');
let socketIO = require('socket.io');
var redis = require('socket.io-redis');
require('./app/helpers/logging')();
var moment = require('moment');
require('./app/helpers/validation')();
var redisc = require('redis');
let redisConfig = {
    port: 6379,
    debug: false,
    dbHost: 'sportredis.gamefeeds.ml',
    dbOptions: {
        auth_pass: 'codebrik#321',
        no_ready_check: true,
    }
};
var Redisclient = redisc.createClient(
    redisConfig.port,
    redisConfig.dbHost,
    redisConfig.dbOptions
);

mongoose.Promise = Promise;
let mongoOptions = {useMongoClient: true, poolSize: process.env.POOL_SIZE || 100};
console.log('Mongo Options: ', mongoOptions);
console.log('config.database.URI,', config.database.URI);
console.log('config.mongoOptions.URI,', mongoOptions);
mongoose.connect(config.database.URI, mongoOptions);

// app.use(cors());
app.use(bodyParser.urlencoded(config.bodyParser.urlencoded));
app.use(bodyParser.json(config.bodyParser.json));
app.use(compression());
app.use(express.static(__dirname + '/public'));

let port = process.env.PORT || 5060;

require('./routes.js')(app);
app.use(error);
var server = app.listen(port);


var allowedOrigins = '*:*';
let io = socketIO(server, {
    origins: allowedOrigins
});
// let io = ();
app.set('socketio', io);
io.set('transports', ['websocket',
    'flashsocket',
    'htmlfile',
    'xhr-polling',
    'jsonp-polling',
    'polling'
]);


//io.adapter(redis({ host: 'localhost', port: 6379 }));
io.adapter(redis({
    host: redisConfig.dbHost,
    port: redisConfig.port,
    auth_pass: redisConfig.dbOptions.auth_pass
}));

io.on('connection', (socket) => {
    // console.log('user connected');


    socket.on('join-room', (roomId) => {
        console.log(">join-room > ", roomId);
        socket.join(roomId);
    });

    socket.on('leave-room', (roomId) => {
        console.log(">leave-room > ", roomId);
        socket.leave(roomId);
    });


    // socket.on('api-obj', (message) => {
    //     console.log(message);
    //     //io.emit('api-obj', message);
    //     io.sockets.emit('api-obj', message);
    // });

    // socket.on('change-status', (status) => {
    //     io.emit('change-status', status);
    // });

    // socket.on('cancelMarket', (status) => {
    //     io.emit('cancelMarket', status);
    // });

    // socket.on('logoutByAdmin', (status) => {
    //     io.emit('logoutByAdmin', status);
    // });

    // socket.on('updateBalancebyAdmin', (status) => {
    //     io.emit('updateBalancebyAdmin', status);
    // });


    socket.on('disconnect', (reason) => {
        console.log("reason> ", reason);
        console.log(`Disconnected: ${error || reason}`);
    });

});


if(config.scheduler.enable) {
    scheduler.start();
}
var j = schedule.scheduleJob('*/1 * * * * *', function (fireDate) {
       // callToCasion();
});

var sett = schedule.scheduleJob('*/1 * * * * *', function (fireDate) {
    samudraSettled();
});


function callToCasion() {
    let aray = [];
    let i = 0 ;
    var timeer = setInterval(function() {
    // var start = moment(new Date()).format("YYYY-MM-DD hh:mm:ss:SSS"); //todays date
        var casinoApiURL = "https://real-game.club/api/exchange/gamesfeeds/casino/getcasinodata";
        requestify.request(casinoApiURL, {
            method: 'POST',
            body: {},
            headers: {
                'Origin': 'https://gamefeeds.ml'
            },
        }).then(function (response ) {
            // var duration = moment.duration(start.diff(end));
            // console.log("dur",duration );
            // console.log("start",start);
            // console.log("end",end);
            // console.log('              ');
            if (response.getCode() == 200) {
                var bodyData = response.getBody();
                if (bodyData.data) {
                    var loopData = bodyData.data;
                    Redisclient.set('casinoData', JSON.stringify(loopData));
                    // loopData.forEach(element => {

                    _.map(loopData , function (element) {
                        var roomCreateForBhav = constants.casinoPrefix + element.eventId;
                        Redisclient.set('casino_'+ element.eventId, JSON.stringify(element));
                        io.sockets.to(roomCreateForBhav).emit('casino-rate', element);
                                    for (var key in element.resultsArr[0].runners) {
                                        if (element.resultsArr[0].runners[key] !== '') {
                                            Redisclient.set('result_'+ element.eventId, JSON.stringify(element));
                                        }
                                        }
                    });
                }
            } else {
                console.log("response======================== > error", JSON.stringify(response));
            }
            i = i + 1;
            if(i == 3){
                clearInterval(timeer);
            }
        }).catch(error => {
        });
    },300);
}

function samudraSettled(){
    let aray = [];
    let i = 0 ;
    var timeerSettled = setInterval(function() {
    // var start = moment(new Date()).format("YYYY-MM-DD hh:mm:ss:SSS"); //todays date
        var resultUrl = "https://t20exchange.com/v1/api/gamefeeds/result/getmarket_result";
        requestify.request(resultUrl, {
            method: 'POST',
            body: {},
            headers: {
                'Origin': 'https://gamefeeds.ml'
            },
        }).then(function (response ) {
            // var duration = moment.duration(start.diff(end));
            // console.log("dur",duration );
            // console.log("start",start);
            // console.log("end",end);
            // console.log('              ');
            if(response.code == 200){
                let result = JSON.parse(response.body);
                if(result.data){
                    let totalRecords = result.data;
                    Redisclient.set("settledResult", JSON.stringify(totalRecords));    
                }
            }
            i = i + 1;
            if(i == 2){
                clearInterval(timeerSettled);
            }
        }).catch(error => {
            Redisclient.set("settledError", JSON.stringify(error));
        });
    },500);
}

console.log('codebrik user running on port ' + port);
