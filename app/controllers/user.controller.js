var app = require('express')();
var http = require('http').createServer(app);
var io = require('socket.io')(http);

let User = require('../models/user.model');
let Helper = require('../helpers/common.helper');
var requestify = require('requestify');
var _ = require('underscore');

io.on('connection', function(socket){
    console.log('a user connected');
    getRate();
  });

module.exports = {

    create: async (req, res, next) => {
            User.create(req.body)
                .then(Helper.respondAsJSON(res))
                .catch(Helper.handleError(res));
    },

    getAll: (req, res, next) => {
        console.log("io");
        console.log(io);
        let query = {};
        io.emit('welcome', { message: "dsds", id: "fddf" });
        let paginator = Helper.getPaginator(req);
        paginator.sort = {priority: -1};
        User.paginate(query, paginator)
            .then(Helper.respondAsJSON(res))
            .catch(Helper.handleError(res));
    },

    get: (req, res, next) => {
        User.findOne({_id: req.params.id})
            .then(Helper.respondAsJSON(res))
            .catch(Helper.handleError(res));
    },

    getCasino: (req, res, next) => {
        var requestArray = {
            method: 'POST',
            headers: {
                Origin: "https://gamefeeds.ml"  // origin
            },
            dataType: 'json',
            data: ''
        };

        let url = 'https://real-game.club/api/exchange/gamesfeeds/casino/getcasinodata';
        requestify.request(url, requestArray)
            .then(function (response) {
                if (response.getCode() == 200) {
                    var datas = JSON.parse(response.body);
                    res.status(200).send({
                        status: true,
                        data: datas.data,
                        message: "Data found.",
                    });
                } else {
                    res.status(200).send({
                        status: false,
                        data: '',
                        message: "Invalid Request. 1",
                    });
                }
            }).catch(error => {
                console.log("response>", error);
                res.status(200).send({
                    status: false,
                    data: JSON.stringify(error),
                    message: "Invalid Request. 2 ",
                });
            });
    },

    update: (req, res, next) => {
        console.log(req.body);
        User.findOneAndUpdate()
            .then(Helper.respondAsJSON(res)
            )
            .catch(Helper.handleError(res));
    },

    delete: (req, res, next) => {
        User.remove({_id: req.params.id})
            .then(Helper.respondAsJSON(res))
            .catch(Helper.handleError(res));
    },

    count: (req, res, next) => {
        User.count({}) //total no. of counts
            .then(counts => {
                res.status(200).send({error: false, count: counts});
            })
            .catch(Helper.handleError(res));
    },

    activeGameSamudra: (req, res, next) => {
        var requestArray = {
            method: 'POST',
            // body: bodyData,
            headers: {
                'Content-Type': 'application/json',
                'Origin' : 'https://gamefeeds.ml'
            },
            dataType: 'json'
        };
        let data = {}
        let url = "https://t20exchange.com/v1/api/fancy/odds/gamefeeds";
        requestify.post(url,data,requestArray)
         .then(function(response) {
                if(response.code == 200){
                    let result = JSON.parse(response.body);
                    let matchList =  _.uniq(result.data, x => x.event_id);
                    let mainObj = {};
                    let i = 0; 
                    _.map( matchList, function( match ) {
                        // console.log(match);
                        let matchId = match.event_id;
                        let srno = match.event_id;
                        
                        let makeObj = {
                             [matchId] : {
                                "game_srno": matchId,
                                "game_name": match.event_name,
                                "marketid": matchId,
                                "eventid": matchId,
                                "game_time" : match.createdAt
                             }
                        }
                        mainObj = {...mainObj, ...makeObj};
                    });
                    res.status(200).send({ status: true, data: JSON.stringify(mainObj) });
                } else {
                    res.status(200).send({ status: false, data: {} });
                }
            })
    },

    activeGameSamudraBM: (req, res, next) => {
        var requestArray = {
            method: 'POST',
            // body: bodyData,
            headers: {
                'Content-Type': 'application/json',
                'Origin' : 'https://gamefeeds.ml'
            },
            dataType: 'json'
        };
        let data = {}
        let url = "https://t20exchange.com/v1/api/bookmakers/odds/gamefeeds";
        requestify.post(url,data,requestArray)
         .then(function(response) {
                if(response.code == 200){
                    let result = JSON.parse(response.body);
                    let matchList =  _.uniq(result.data, x => x.event_id);
                    let mainObj = {};
                    let i = 0;  
                    _.map( matchList, function( match ) {
                        // console.log(match);
                        let runnerObj = match.manual_data.runnersData;
                        let matchId = match.event_id;
                        let srno = match.event_id;
                        let obj = {
                            "game_srno": matchId,
                            "game_name": match.event_name,
                            "marketid": matchId,
                            "eventid": matchId,
                            "game_time" : match.market_time
                         };
                         obj = {...runnerObj, ...obj};
                        let makeObj = {
                             [matchId] : obj
                        }
                        mainObj = {...mainObj, ...makeObj};
                        i = i + 1;
                        if(i == matchList.length){
                            res.status(200).send({ status: true, data: JSON.stringify(mainObj) });
                        }
                    });
                } else {
                    res.status(200).send({ status: false, data: {} });
                }
            })
    },

    activeMarketSamudra: (req, res, next) => {
        var requestArray = {
            method: 'POST',
            // body: bodyData,
            headers: {
                'Content-Type': 'application/json',
                'Origin' : 'https://gamefeeds.ml'
            },
            dataType: 'json'
        };
        let data = {}
        let url = "https://t20exchange.com/v1/api/fancy/odds/gamefeeds";
        requestify.post(url,data,requestArray)
         .then(function(response) {
                if(response.code == 200){
                    let result = JSON.parse(response.body);
                    let fancyList =  _.filter(result.data, {event_id: req.params.id});
                    let mainObjFancy = {};
                    let i = 0; 
                    _.map( fancyList, function( match ) {
                        let matchId = match.fancy_market_id;
                        // console.log(match);
                        let makeObj = {
                             [matchId] : {
                                "srno": match.fancy_market_id,
                                "game_srno": match.event_id,
                                "event_name":match.fancy_market_name,
                                "display_srno" :match.sequence,
                                "event_type": "Regular",
                                "created": match.createdAt,
                                "betdelay": match.delay_second,
                                "commission": "N",
                                "void_status": (match.is_void==0) ? 'N' : 'Y',
                                "end_status": "N",
                                "result_change": "",
                                "result": "" 
                             }
                        }
                        mainObjFancy = {...mainObjFancy, ...makeObj};
                        i = i + 1;                   
                        if(i == fancyList.length){
                            res.status(200).send({ status: true, data: JSON.stringify(mainObjFancy) });
                        }
                    });
                } else {
                    res.status(200).send({ status: false, data: {} });
                }
            })
    },



    activeMarketSamudraBM: (req, res, next) => {
        var requestArray = {
            method: 'POST',
            // body: bodyData,
            headers: {
                'Content-Type': 'application/json',
                'Origin' : 'https://gamefeeds.ml'
            },
            dataType: 'json'
        };
        let data = {}
        let url = "https://t20exchange.com/v1/api/bookmakers/odds/gamefeeds";
        requestify.post(url,data,requestArray)
         .then(function(response) {
                if(response.code == 200){
                    let result = JSON.parse(response.body);
                    let fancyList =  _.filter(result.data, {event_id: req.params.id});
                    let mainObjFancy = {};
                    console.log(fancyList.length);
                    _.map( fancyList, function( match ) {
                        let matchId = match.market_id;
                        let runnerObj = match.manual_data.runnersData;
                        // console.log(match);
                        let obj = {
                            "srno": match.market_id,
                            "game_srno": match.event_id,
                            "event_name":match.market_name,
                            "display_srno" :"1",
                            "event_type": "ODD",
                            "created": match.createdAt,
                            "betdelay": match.manual_data.betDelay,
                            "commission": "N",
                            "void_status": (match.is_void==0) ? 'N' : 'Y',
                            "end_status": "N",
                            "result_change": "",
                            "result": "" 
                         }
                        //  obj = {...runnerObj, ...obj};
                        let makeObj = {
                             [matchId] : obj
                        }
                        mainObjFancy = {...mainObjFancy, ...makeObj};
                    });
                        res.status(200).send({ status: true, data: JSON.stringify(mainObjFancy) });
                } else {
                    res.status(200).send({ status: false, data: {} });
                }
            })
    },
    
    settledMarketSamudra : (req, res, next) => {
        var requestArray = {
            method: 'POST',
            // body: bodyData,
            headers: {
                'Content-Type': 'application/json',
                'Origin' : 'https://gamefeeds.ml'
            },
            dataType: 'json'
        };
        let data = {}
        let url = "https://t20exchange.com/v1/api/gamefeeds/result/getmarket_result";
        requestify.post(url,data,requestArray)
         .then(function(response) {
                if(response.code == 200){
                    let result = JSON.parse(response.body);
                    res.status(200).send({ status: true, data: result });
                }else{
                    res.status(200).send({ status: false, data: [] });
                }
        }).catch(err => {
                Redisclient.set("settledError", JSON.stringify(match));
                res.status(200).send({ status: false, data: [] });
        });
    },

    getMarketRateSamudra : (req, res, next) => {
        console.log("req.body++++",req.body);
        var requestArray = {
            method: 'POST',
            // body: bodyData,
            headers: {
                'Content-Type': 'application/json',
                'Origin' : 'https://gamefeeds.ml'
            },
            dataType: 'json'
        };
        let data = {
            marketId : req.body.marketId
        }
        let url = "https://t20exchange.com/v1/api/gamefeeds/result/getMarketResultMarket";
        requestify.post(url,data,requestArray)
         .then(function(response) {
                if(response.code == 200){
                    let result = JSON.parse(response.body);
                    res.status(200).send({ status: true, data: result });
                }else{
                    res.status(200).send({ status: false, data: [] });
                }
        })
    },

    addFancy: (req, res, next) => {
        console.log("req.body++++++++++++",req.body);
        var allerrors = [];


        var allerrors = [];
        req.checkBody("fancyId", "fancyId mandatory filed.").notEmpty();
        req.checkBody("fancyName", "fancyName mandatory filed.").notEmpty();
        req.checkBody('fancyStartTime', "fancyStartTime Time name.").notEmpty();
        req.checkBody('fancymarketId', "fancymarketId required.").notEmpty();

        var errors = req.validationErrors();
        if (errors) {
            errors.forEach(element => {
                allerrors.push(element.msg);
            });
            res.status(200).send({
                'status': false,
                'data': '',
                "message": allerrors
            });
        } else {
            var fancyId = req.body.fancyId;

            Redisclient.get("fancyAllList", function (err, response) {
                if (err) throw err;
                if (_.isNull(response)) {
                    var match = [];
                    match.push(fancyId);
                    Redisclient.set("fancyAllList", JSON.stringify(match));
                    addingFancyToList(req.body);
                    res.status(200).send({
                        'status': true,
                        'data': '',
                        "message": "added successfully."
                    });
                } else {
                    response = JSON.parse(response);
                    if (_.indexOf(response, fancyId) > -1) {
                        res.status(200).send({
                            'status': true,
                            'data': '',
                            "message": "already added.."
                        });
                    } else {
                        response.push(fancyId);
                        Redisclient.set("fancyAllList", JSON.stringify(response));
                        addingFancyToList(req.body);
                        res.status(200).send({
                            'status': true,
                            'data': '',
                            "message": "added successfully."
                        });
                    }
                }
            });
        }
    },

    removeFancy: (req, res, next) => {
        var allerrors = [];
        req.checkParams("id", "fancyID mandatory filed.").notEmpty();
        var errors = req.validationErrors();
        if (errors) {
            errors.forEach(element => {
                allerrors.push(element.msg);
            });
            res.status(200).send({
                'status': false,
                'data': '',
                "message": allerrors
            });
        } else {
            var fancyId = req.params.id;

            Redisclient.get("fancyAllList", function (err, response) {
                if (err) throw err;
                if (_.isNull(response)) {
                    removingFancyFromList(fancyId);
                    Helpers.addDeletedFancyId(fancyId, "fancyId");
                    res.status(200).send({
                        'status': true,
                        'data': '',
                        "message": "removed successfully."
                    });
                } else {
                    response = JSON.parse(response);

                    if (_.indexOf(response, fancyId) > -1) {
                        response.splice(response.indexOf(fancyId), 1);
                        Redisclient.set("fancyAllList", JSON.stringify(response));
                        removingFancyFromList(fancyId);
                        Helpers.addDeletedFancyId(fancyId, "fancyId");
                        res.status(200).send({
                            'status': true,
                            'data': '',
                            "message": "removed successfully."
                        });
                    } else {
                        res.status(200).send({
                            'status': true,
                            'data': '',
                            "message": "not listed.x"
                        });
                    }

                }
            });
        }
    },


    getAllFancyList: (req, res, next) => {

        Redisclient.get("fancyAllListDetails", function (err, response) {
            if (err) throw err;
            if (_.isNull(response)) {
                res.status(200).send({
                    'status': false,
                    'data': [],
                    "message": "no data found."
                });
            } else {
                res.status(200).send({
                    'status': true,
                    'data': JSON.parse(response),
                    "message": "list successfully."
                });

            }
        });
    },
    getAllFancyListx: (req, res, next) => {

        Redisclient.get("fancyAllList", function (err, response) {
            if (err) throw err;
            if (_.isNull(response)) {
                res.status(200).send({
                    'status': true,
                    'data': [],
                    "message": "No Data Found."
                });
            } else {
                res.status(200).send({
                    'status': true,
                    'data': JSON.parse(response),
                    "message": "list successfully."
                });

            }
        });
    },

    add: (req, res, next) => {
        var allerrors = [];


        var allerrors = [];
        req.checkBody("marketId", "ID mandatory filed.").notEmpty();
        req.checkBody("matchId", "matchId mandatory filed.").notEmpty();
        req.checkBody("marketName", "Name mandatory filed.").notEmpty();
        req.checkBody('marketStartTime', "Time name.").notEmpty();

        var errors = req.validationErrors();



        if (errors) {
            errors.forEach(element => {
                allerrors.push(element.msg);
            });
            res.status(200).send({
                'status': false,
                'data': '',
                "message": allerrors
            });
        } else {
            var marketId = req.body.marketId;
            var matchId = req.body.matchId;

            Redisclient.get("matchAllList", function (err, response) {
                if (err) throw err;
                if (_.isNull(response)) {
                    activateMatch(marketId);
                    var match = [];
                    match.push(marketId);
                    Redisclient.set("matchAllList", JSON.stringify(match));
                    addingToList(req.body);
                    res.status(200).send({
                        'status': true,
                        'data': '',
                        "message": "added successfully."
                    });
                } else {
                    response = JSON.parse(response);
                    if (_.indexOf(response, marketId) > -1) {
                        res.status(200).send({
                            'status': true,
                            'data': '',
                            "message": "already added.."
                        });
                    } else {
                        activateMatch(marketId);
                        response.push(marketId);
                        Redisclient.set("matchAllList", JSON.stringify(response));
                        addingToList(req.body);
                        res.status(200).send({
                            'status': true,
                            'data': '',
                            "message": "added successfully."
                        });
                    }
                }
            });
        }
    },

    remove: (req, res, next) => {

        var allerrors = [];
        req.checkParams("id", "Match mandatory filed.").notEmpty();
        var errors = req.validationErrors();
        if (errors) {
            errors.forEach(element => {
                allerrors.push(element.msg);
            });
            res.status(200).send({
                'status': false,
                'data': '',
                "message": allerrors
            });
        } else {
            var matchId = req.params.id;

            Redisclient.get("matchAllList", function (err, response) {
                if (err) throw err;
                if (_.isNull(response)) {

                    //do not chagne the order for code  // No.1 
                    Helpers.addDeletedFancyId(matchId, "matchId"); // No.2 
                    setTimeout(function () { Helpers.addDeletedmatchId(matchId); }, 300); // No.3
                    setTimeout(function () { Helpers.removeFancyfromOrigin(matchId); }, 700); // No.4
                    setTimeout(function () { Helpers.removingFromList(matchId); }, 900); // No.5

                    res.status(200).send({
                        'status': true,
                        'data': '',
                        "message": "removed successfully."
                    });
                } else {
                    response = JSON.parse(response);

                    // if (_.indexOf(response, matchId) > -1) {
                    console.log("XYZ matchId> ", matchId);

                    response.splice(response.indexOf(matchId), 1);
                    Redisclient.set("matchAllList", JSON.stringify(response));

                    //do not chagne the order for code  // No.1 
                    Helpers.addDeletedFancyId(matchId, "matchId"); // No.2
                    setTimeout(function () { Helpers.addDeletedmatchId(matchId); }, 300); // No.3
                    setTimeout(function () { Helpers.removeFancyfromOrigin(matchId); }, 700); // No.4
                    setTimeout(function () { Helpers.removingFromList(matchId); }, 900); // No.5

                    res.status(200).send({
                        'status': true,
                        'data': '',
                        "message": "removed successfully."
                    });
                    // } else {
                    //     res.status(200).send({
                    //         'status': true,
                    //         'data': '',
                    //         "message": "not listed.z"
                    //     });
                    // }

                }
            });
        }
    },
};