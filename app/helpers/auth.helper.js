'use strict';
let Helper = require('../helpers/common.helper');
let Constants = require(`../../config/${process.env.CONST_FILE}.js`);
let request = require('request-promise');

function getAccessTokenFromHeader(req) {
  return (req.headers['authorization'] && req.headers['authorization']!==null?req.headers['authorization'].split(' ')[1]:null);
}

function getAccessTokenFromUrl(req) {
  return (req.query.hasOwnProperty('Authorization') && Helper.getObject(req.query.Authorization)!==null?req.query.Authorization:null);
}

module.exports = {

  /**
   * Get Token from Request Header
   * @param req
   * @returns String
   
   */
  getAccessToken: (req) => {
    return getAccessTokenFromHeader(req) || getAccessTokenFromUrl(req);
  },

  /**
   * Get Access Token using Admin credentials
   * @returns Promise
   
   */
  getAdminAccessToken: () => {
    return new Promise((resolve, reject) => {
      let options = {
        url: Constants.oauthTokenURL,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'charset': 'utf-8',
          'Authorization': 'key'
        },
        method: 'POST',
        body: Constants.tokenBody
      };
      request(options)
        .then(response => {
          resolve(JSON.parse(response).access_token);
        }).catch(err => {
        reject(err);
        console.error(err);
      })
    });
  },

  /**
   * OAuth 2 Token Validation
   * @param req
   * @param res
   * @param next
   * @returns Boolean
   
   */
  ensure: (req, res, next) => {
    let token = getAccessTokenFromHeader(req) || getAccessTokenFromUrl(req);
    let tokenErr = {};
    tokenErr.error = 'invalid_token';
    tokenErr.error_description = 'Invalid or no access token provided.';

    if (token && token!==null) {
      let options = {
        url:'url or ',
        method: 'GET',
        headers: {
          'Authorization': 'jWt key',
          'Content-Type': 'application/json'
        }
      };
      request(options)
        .then(() => {
          next();
          return false;
        })
        .catch(err => {
          console.log(err);
          res.status(401).send(tokenErr);
        })
    } else {
      res.status(401).send(tokenErr);
    }
  }
};