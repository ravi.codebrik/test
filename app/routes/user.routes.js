let express = require('express');
let router = express.Router();
let UserController  = require('../controllers/user.controller');
let Authentication = require('../helpers/auth.helper');
router.get('/active-game-samudra', UserController.activeGameSamudra);
router.get('/active-market-samudra/:id', UserController.activeMarketSamudra);

router.get('/active-game-samudra-bm', UserController.activeGameSamudraBM);
router.get('/active-market-samudra-bm/:id', UserController.activeMarketSamudraBM);
router.get('/settled-market-samudra', UserController.settledMarketSamudra);

router.get('/getcasino',/* Authentication.ensure, */ UserController.getCasino);


//edition by peter
router.get('/remove-fancy/:id', /*Authentication.ensure,*/ UserController.removeFancy);
router.get('/get-fancy/all', /*Authentication.ensure,*/ UserController.getAllFancyList);
router.get('/get-fancy/allx', /*Authentication.ensure,*/ UserController.getAllFancyListx);
// router.get('/get-score/:id', /*Authentication.ensure,*/ UserController.getScore);
router.get('/remove/:id', /*Authentication.ensure,*/ UserController.remove);


router.get('/', /*Authentication.ensure,*/ UserController.getAll);
router.get('/count', /*Authentication.ensure,*/ UserController.count);
router.post('/add-fancy', /*Authentication.ensure,*/ UserController.addFancy);
router.post('/get-market-result-samudra', /*Authentication.ensure,*/ UserController.getMarketRateSamudra);

router.post('/', /*Authentication.ensure,*/UserController.create);
router.post('/add', /*Authentication.ensure,*/ UserController.add);
router.get('/:id',/* Authentication.ensure, */UserController.get);
router.put('/:id',/* Authentication.ensure,*/ UserController.update);
router.delete('/:id',/* Authentication.ensure, */UserController.delete);


module.exports = router;